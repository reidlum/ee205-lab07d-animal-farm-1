///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file addCats.h
/// @version 1.0
///
/// @author Reid Lum <reidlum@hawaii.edu>
/// @date 14_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#pragma once

extern int addCat(char* inputname, enum genders inputgender, enum breeds inputbreed, bool isFixed, float inputweight, enum Color inputcollarColor1, enum Color inputcollarColor2, unsigned long long inputlicense);
