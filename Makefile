###############################################################################
### University of Hawaii, College of Engineering
### @brief Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
###
### @file Makefile
### @version 1.0 - Initial version
###
### Build and test an ANIAML FARM
###
### @author Reid Lum <reidlum@hawaii.edu>
### @date 14_Feb_2022
###
### @see https://www.gnu.org/software/make/manual/make.html
###############################################################################
CC = gcc
CFLAGS = -g -Wall -Wextra
TARGET = animalfarm
all: $(TARGET)

main.o: main.c catDatabase.h addCats.h reportCats.h updateCats.h deleteCats.h config.h
	$(CC) $(CFLAGS) -c main.c

catDatabase.o: catDatabase.c catDatabase.h config.h
	$(CC) $(CFLAGS) -c catDatabase.c

addCats.o: addCats.c addCats.h catDatabase.h config.h
	$(CC) $(CFLAGS) -c addCats.c

reportCats.o: reportCats.c reportCats.h catDatabase.h config.h
	$(CC) $(CFLAGS) -c reportCats.c

updateCats.o: updateCats.c updateCats.h catDatabase.h config.h
	$(CC) $(CFLAGS) -c updateCats.c

deleteCats.o: deleteCats.c deleteCats.h catDatabase.h config.h
	$(CC) $(CFLAGS) -c deleteCats.c

animalfarm: main.o catDatabase.o addCats.o reportCats.o updateCats.o deleteCats.o
	$(CC) $(CFLAGS) -o $(TARGET) main.o catDatabase.o addCats.o reportCats.o updateCats.o deleteCats.o

clean:
	rm -f $(TARGET) *.o

test: $(TARGET)
	./$(TARGET)
